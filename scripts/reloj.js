'use strict';

(() => {
    let actualizaReloj = () => {
        let fecha = new Date();
        let fechaFormateada = fecha.toLocaleString('es-ES');

        let reloj = document.getElementById('reloj');
        reloj. value = fechaFormateada;
    };

    window.addEventListener('DOMContentLoaded', () => {
        setInterval(actualizaReloj, 1000);
    });
})();