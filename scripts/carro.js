'use strict';

(() => {
    let generaArticulos = () => {
        let jsonArticulos = getArticulos();
        let articulos = JSON.parse(jsonArticulos);
        let contenedorArticulos = document.getElementById('item_container');

        articulos.data.forEach(articulo => {
            contenedorArticulos.innerHTML += `
            <div class="item" id="i` + articulo.id + `">
			<img src="` + articulo.imagen + `" alt="descripción i2">
			<label class="title">` + articulo.nombre + `</label>
			<label class="price">` + articulo.precio + ` €</label>
			<label class="stock">Stock ` + articulo.stock + `</label>
            </div>
            `;
        });
        contenedorArticulos.innerHTML += '<div class="clear"></div>';

        console.log(JSON.stringify(articulos));
    };

    let obtenerImporte = (textoImporte) => Number(textoImporte.split(' ')[0]);

    let actualizaStock = (articulo, valor) => {
        let stock = articulo.querySelector('.stock');

        let arrStock = stock.innerText.split(' ');
        let valorStockAnterior = Number(arrStock[1]);
        let valorStock = valorStockAnterior + valor; 
        stock.innerText = arrStock[0] + ' ' + valorStock;

        if (valorStock === 0)
            stock.classList.add('agotado');

        if (valorStockAnterior === 0)
            stock.classList.remove('agotado');
    };

    let actualizaCompras = (valor) => {
        let citem = document.getElementById('citem');
        citem.value = Number(citem.value)+valor;
    }

    let eliminaArticuloDelCarrito = (articulo) => {
        let idArticuloLista = articulo.id.replace('c', '');
        let articuloLista = document.getElementById(idArticuloLista);
        actualizaStock(articuloLista, 1);    
        actualizaCompras(-1);    
        actualizaPrecioTotal(articulo, false);
        articulo.remove();
    };

    let actualizaPrecioTotal = (articulo, estoyAñadiendo) => {
        let price = articulo.querySelector('.price');
        let precio = obtenerImporte(price.innerText);
        if (!estoyAñadiendo)
            precio *= -1;
        let cprice = document.getElementById('cprice');
        cprice.value = (obtenerImporte(cprice.value)+precio) + ' €';
    };

    let insertaArticuloEnCarrito = (articulo) => {
    
        let copia = articulo.cloneNode(true);
    
        copia.id = 'c' + articulo.id;
        copia.querySelector('.stock').style.display = 'none';
        copia.style.cursor = 'default';
        copia.childNodes.forEach(hijo => {
            if (hijo.nodeType === Node.ELEMENT_NODE) 
                hijo.style.cursor = 'default'
        });
    
        let enlace = document.createElement('a');
        enlace.href = '';
        enlace.className = 'delete';

        enlace.addEventListener('click', function (event) {
            event.preventDefault();
            eliminaArticuloDelCarrito(this.parentNode);
        });

        copia.insertBefore(enlace, copia.firstChild);
    
        let cartItems = document.getElementById('cart_items');
        cartItems.insertBefore(copia, cartItems.firstChild);
    
        actualizaStock(articulo, -1);
    
        actualizaCompras(1);
    
        actualizaPrecioTotal(articulo, true);
    };

    let listaMensajes = document.getElementById('lista');
    let intervalo=null;

    let mostrandoLista = () => {
        let style = window.getComputedStyle(listaMensajes, '');
        let altoLista = Number(style.height.replace('px', ''));

        if (altoLista < 300) {
            altoLista += 10;
        }
        else {
            altoLista = 300;
            clearInterval(intervalo);
            intervalo=null;
        }
        listaMensajes.style.height = altoLista + 'px';
    };

    let mostrarMensajes = () => {

        if (intervalo === null)
            intervalo = setInterval(mostrandoLista, 50);
    };

    let ocultandoLista = () => {
        let style = window.getComputedStyle(listaMensajes, '');
        let altoLista = Number(style.height.replace('px', ''));

        if (altoLista > 0) {
            altoLista -= 10;
        }
        else {
            altoLista = 0;
            clearInterval(intervalo);
            intervalo=null;
        }
        listaMensajes.style.height = altoLista + 'px';
    };

    let ocultarMensajes = () => {
        if (intervalo === null)
            intervalo = setInterval(ocultandoLista, 50);
    };
    
    window.addEventListener('DOMContentLoaded', () => {
        generaArticulos();
        let articulos = document.querySelectorAll('.item');
        let botonVaciar = document.getElementById('btn_clear');
        let botonMostrarMensajes = document.getElementById('bt_mostrar');
        let botonOcultarMensajes = document.getElementById('bt_ocultar');

        articulos.forEach(articulo => articulo.addEventListener('dblclick', function () {
            let articulo = this;
            if (!articulo.querySelector('.stock').classList.contains('agotado')) 
                insertaArticuloEnCarrito(this);
            
        }));

        botonVaciar.addEventListener('click', function () {
            let enlacesEliminar = document.querySelectorAll('.delete');
            enlacesEliminar.forEach(enlaceEliminar => enlaceEliminar.click() );
        });

        botonMostrarMensajes.addEventListener('click', function () {
            mostrarMensajes();
        });

        botonOcultarMensajes.addEventListener('click', function () {
            ocultarMensajes();
        });
    });
})();